# Docker and docker-compose

```
# Software used:
# Docker version 20.10.7, build 20.10.7-0ubuntu1~20.04.1
# docker-compose version 1.28.3, build 14736152

# enter directory in working copy
cd docker

# build images
docker-compose build

# run containers and show output
docker-compose up -d && docker-compose logs -f

# destroy containers
docker-compose down
```

# Credential path /private

```
# password can be changed in docker/proxy/bin/start.sh

# TODO: need to be handled using environment variables
u: admin
p: 1q2w3e
```

# Kubernets

```
# This was tested under Ubuntu 20.04 LTS

# enter directory in working copy
cd k8s

# Install microk8s
# https://microk8s.io/docs
sudo snap install microk8s --classic --channel=1.21
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
su - $USER
microk8s status --wait-ready

microk8s enable dns
microk8s enable metrics-server

# docker-compose should be executed at least one time to build the images
docker save clm-api > ../data/clm-api.tar
docker save clm-proxy > ../data/clm-proxy.tar
microk8s ctr image import ../data/clm-api.tar
microk8s ctr image import ../data/clm-proxy.tar

# Create namespace
# https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-organizing-with-namespaces
microk8s.kubectl create -f namespace.yml

# Configure deployment and services
microk8s.kubectl apply -f api.yml
microk8s.kubectl apply -f proxy.yml

# autoscaler
https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/

# Delete autoscaler
microk8s.kubectl delete hpa -n clm api

# Delete deployment and services
microk8s.kubectl delete -n clm service proxy
microk8s.kubectl delete -n clm deployment proxy
microk8s.kubectl delete -n clm service api
microk8s.kubectl delete -n clm deployment api

# Get proxy IP and test the application
microk8s.kubectl get svc --namespace clm

microk8s.kubectl get deployments --namespace clm
microk8s.kubectl get pods --output=wide --namespace clm
```

# Terraform & RBAC

```
https://kubernetes.io/docs/reference/access-authn-authz/rbac/
https://www.youtube.com/watch?v=BLktpM--0jA
https://microk8s.io/docs/multi-user
https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user

# Software used
# Terraform v1.0.3 on linux_amd64
# provider registry.terraform.io/hashicorp/kubernetes v2.4.1

# enter directory in working copy
cd terraform

# Enable rbac
microk8s enable rbac

# Add k8s normal user
# https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user

# Create private key
openssl genrsa -out ../data/clm2.key 2048
openssl req \
  -subj "/C=CL/ST=Santiago/L=Santiago/O=Company Name/OU=Org/CN=clm2" \
  -new \
  -key ../data/clm2.key \
  -out ../data/clm2.csr

# Create certificate signing request
cat <<EOF | microk8s.kubectl --validate=false apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: clm2
spec:
  request: $(cat ../data/clm2.csr | base64 | tr -d "\n")
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
EOF

# Show signing request
microk8s.kubectl get csr

# Approve signing request
microk8s.kubectl certificate approve clm2

# Get certificate of user
microk8s.kubectl get csr clm2 -o jsonpath='{.status.certificate}'| base64 -d > ../data/clm2.crt

# Add user to kubeconfig
microk8s.kubectl config set-credentials clm2 --client-key=../data/clm2.key --client-certificate=../data/clm2.crt --embed-certs=true

# Add context for normal user
microk8s.kubectl config set-context clm2 --cluster=microk8s-cluster --user=clm2

# create kubeconfig of admin user, used by terraform provider
microk8s.kubectl config view --raw > $HOME/.kube/config

# Create role and binding
terraform init
terraform plan
terraform apply

# switch to user context
microk8s.kubectl config use-context clm2

# try to show pods in default namespace, it will give an error
microk8s.kubectl get pods --output=wide

# show pods in clm namespace
microk8s.kubectl get pods --output=wide --namespace clm

# back to admin context
microk8s.kubectl config use-context microk8s
```

# Makefile

 Assumptions:
 - docker, docker-compose, microk8s y terraform are installed in the host and  all the executables are availables in the path of the user
 - the user has admin privilegies in microk8s
 - There is a user called `clm2` in k8s

```
make build kube
```
