help:
	@echo ''
	@echo 'Usage: make [TARGET] [EXTRA_ARGUMENTS]'
	@echo 'Targets:'
	@echo '  build: build docker images'
	@echo '  kube: create kubernetes resources'
	@echo ''

build:
	cd docker && \
	docker-compose build

kube:
	@echo "\n*** Enable dns, metrics-server and rbac in microk8s\n"
	microk8s enable dns
	microk8s enable metrics-server
	microk8s enable rbac
	@echo "\n*** Save and import docker images to microk8s\n"
	docker save clm-api > ./data/clm-api.tar
	docker save clm-proxy > ./data/clm-proxy.tar
	microk8s ctr image import ./data/clm-api.tar
	microk8s ctr image import ./data/clm-proxy.tar
	@echo "\n*** Create deployments, services and autoscaler\n"
	cd k8s && \
	microk8s.kubectl apply -f namespace.yml && \
	microk8s.kubectl apply -f api.yml && \
	microk8s.kubectl apply -f proxy.yml
	@echo "\n*** Create role and binding to user clm2\n"
	cd terraform && \
	microk8s.kubectl config view --raw > ${HOME}/.kube/config && \
	terraform init && \
	terraform apply
