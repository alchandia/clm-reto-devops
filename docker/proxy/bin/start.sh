#!/bin/bash

echo "*** Check if SSL cert exists"
if [ ! -f /etc/nginx/ssl/ssl.crt ] ; then

  mkdir -p /etc/nginx/ssl
  echo "*** Create default SSL cert"
  openssl req \
    -subj "/C=CL/ST=Santiago/L=Santiago/O=Company Name/OU=Org/CN=localhost" \
    -new -newkey rsa:2048 \
    -sha256 \
    -days 365 \
    -nodes \
    -x509 \
    -keyout /etc/nginx/ssl/ssl.key \
    -out /etc/nginx/ssl/ssl.crt

fi

htpasswd -cb /etc/nginx/.htpasswd admin 1q2w3e

echo "*** Starts nginx daemon"
nginx -g 'daemon off;'
