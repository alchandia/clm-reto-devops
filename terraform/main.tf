terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

module "role-clm-pods" {
  source = "./modules/k8s-role"
  name = "role-pods"
  namespace = "clm"
  users = ["clm2"]
}
