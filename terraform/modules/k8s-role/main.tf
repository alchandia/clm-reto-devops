resource "kubernetes_role" "role" {
  metadata {
    annotations = {}
    labels = {}
    name = var.name
    namespace = var.namespace
  }
  rule {
    api_groups = var.api_groups
    resource_names = var.resource_names
    resources = var.resources
    verbs = var.verbs
  }
}

resource "kubernetes_role_binding" "role-bind" {
  metadata {
    annotations = {}
    labels = {}
    name = "role-bind-${var.name}-${var.namespace}"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role"
    name = var.name
  }
  dynamic "subject" {
    for_each = var.users
    content {
      kind = "User"
      name = subject.value
      api_group = "rbac.authorization.k8s.io"
    }
  }

}
