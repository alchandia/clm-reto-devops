# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "name" {
  description = "Role name"
  type = string
}

variable "namespace" {
  description = "Namespace asociated with role"
  type = string
}

variable "users" {
  description = "Users that will be associated with the role"
  type = list(string)
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "api_groups" {
  description = "..."
  type = list(string)
  default = [""]
}

variable "resource_names" {
  description = "..."
  type = list(string)
  default = [""]
}

variable "resources" {
  description = "..."
  type = list(string)
  default = ["pods"]
}

variable "verbs" {
  description = "..."
  type = list(string)
  default = ["get", "list", "watch"]
}
